# COMP3931 - Individual Project - sc18ljp

This repository contains code written for the COMP3931 Individual Project module by Laurence Palmer (sc18ljp).

It is a NodeJS web application for simulating the evacuation of pedestrians using cellular automata.

## Installation

Steps to install and run the application are as follows:

1. Clone this repository.
1. Run `npm install` from the root of this repository to install the necessary dependencies.
1. Run `npm run build` to build the application.
1. Run `npm run start` to start the application. The application is then accessible through a web browser at port 8000 unless another is specified in the `PORT` environment variable.

## Licenses

### Project License

The license for this project can be found in `license.txt`.

### 3rd Party Licenses

The licenses associated with this project's dependencies can be found in `licences.txt`.
