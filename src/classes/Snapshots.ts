import Cell from "./Cell";
import { ClientState } from "./States";

/**
 * Representation of a configuration at a certain point in time.
 */
export type ConfigurationSnapshot = {
  width: number;
  height: number;
  doors: Cell[];
  obstacles: Cell[];
  pedestrians: Cell[];
  panic: number;
};

/**
 * Representation of the application at a certain point in time that is to be
 * sent to the client.
 */
export type ClientSnapshot = {
  state: ClientState;
  width: number;
  height: number;
  doors: Cell[];
  obstacles: Cell[];
  pedestrians: Cell[];
  panic: number;
  time: number;
  pedestrianCountsAtTime: number[][];
  averageTime: number | null;
  interval: number;
};
