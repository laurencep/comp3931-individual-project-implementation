/**
 * An interface to the Event class limiting the methods exposed to subscribe and
 * unsubscribe.
 */
export interface IEvent {
  subscribe(handler: { (): void }): void;
  unsubscribe(handler: { (): void }): void;
}

/**
 * Represents an event that can occur.
 */
export default class Event implements IEvent {
  private _handlers: { (): void }[] = []; // Array of event handler functions.

  /**
   * Adds the given function to the event handlers for this event resulting in
   * it being called whenever the event occurs.
   * @param handler Function to run when event occurs.
   */
  subscribe(handler: { (): void }): void {
    // Add function to handler array.
    this._handlers.push(handler);
  }

  /**
   * Removes the given function from the event handlers for this event.
   * @param handler Function currently acting as a handler to remove
   */
  unsubscribe(handler: { (): void }): void {
    // Update event handlers, filtering out the given function.
    this._handlers = this._handlers.filter((h) => h !== handler);
  }

  /**
   * Calls the event handlers for this event. To be called when this event
   * occurs.
   */
  trigger(): void {
    this._handlers.forEach((h) => h());
  }

  /**
   * Returns an interface that exposes the subscribe and unsubscribe methods of
   * this event.
   * When a class has a private field event, the corresponding public property
   * should return the interface provided by this method to prevent outside
   * access to the trigger method.
   * @returns Interface of this event allowing functions to subscribe and
   * unsubscribe from this event.
   */
  expose(): IEvent {
    return this;
  }
}
