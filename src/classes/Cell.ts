import Point from "./Point";

/**
 * Represents a cell in a 2D area defined by integer x and y coordinates.
 */
export default class Cell extends Point {
  /**
   * Creates a new cell from a set of x and y integer coordinates.
   * @param x x coordinate.
   * @param y y coordinate.
   */
  constructor(x: number, y: number) {
    // Check coordinates are integer, throw error if not.
    if (Number.isInteger(x) && Number.isInteger(y)) {
      super(x, y);
    } else {
      throw new TypeError("Cell coordinates must be integers.");
    }
  }

  /**
   * Creates a new cell from a point.
   * @param p Point to create cell from.
   * @param ppc: The number of points per cell.
   * @returns Cell that p is contained by.
   */
  static fromPoint(p: Point, ppc: number): Cell {
    // Round down point coordinates and return new cell.
    return new Cell(Math.floor(p.x / ppc), Math.floor(p.y / ppc));
  }

  /**
   * Calculates the Manhattan distance between two cells.
   * i.e. the sum of the differences of the x and y coordinates.
   * (https://en.wikipedia.org/wiki/Taxicab_geometry)
   * @param c1 First cell.
   * @param c2 Second cell.
   * @returns Manhattan distance between c1 and c2.
   */
  static manhattanDistance(c1: Cell, c2: Cell): number {
    // Calculate and return Manhattan distance between the two cells.
    return Math.abs(c1.x - c2.x) + Math.abs(c1.y - c2.y);
  }

  /**
   * Finds the cells which are directly adjacent, orthogonally and diagonally to
   * a given cell.
   * @param w Width of the area containing the cells.
   * @param h Height of the area containing the cells.
   * @param c Cell of which to find adjacent cells.
   * @returns Array of cells adjacent to c.
   */
  static getAdjacentCellsOfCell(w: number, h: number, c: Cell): Cell[] {
    // Generate array of all adjacent cells.
    let adjacentCells: Cell[] = [];
    adjacentCells.push(new Cell(c.x, c.y - 1)); // up
    adjacentCells.push(new Cell(c.x + 1, c.y - 1)); // up-right
    adjacentCells.push(new Cell(c.x + 1, c.y)); // right
    adjacentCells.push(new Cell(c.x + 1, c.y + 1)); // down-right
    adjacentCells.push(new Cell(c.x, c.y + 1)); // down
    adjacentCells.push(new Cell(c.x - 1, c.y + 1)); //down-left
    adjacentCells.push(new Cell(c.x - 1, c.y)); // left
    adjacentCells.push(new Cell(c.x - 1, c.y - 1)); // up-left

    // Filter out cells that are outside of the given area and return results.
    return adjacentCells.filter(
      (c) => c.x >= 0 && c.x < w && c.y >= 0 && c.y < h
    );
  }

  /**
   * Finds the cells which are directly adjacent, orthogonally and diagonally to
   * a given cell, that have a given value.
   * @param a 2D array containing the values of each cell in the area.
   * @param c Cell of which to find adjacent cells with a certain value.
   * @param v Value of which to find adjacent cells with.
   * @returns Array of cells adjacent to c that have value v.
   */
  static getAdjacentCellsOfCellWithValue<Type>(
    a: Type[][],
    c: Cell,
    v: Type
  ): Cell[] {
    // Find the cells adjacent to the given cell.
    let adjacentCells: Cell[] = this.getAdjacentCellsOfCell(
      a.length,
      a[0].length,
      c
    );

    // Filter the results for cells matching a given value and return results.
    return adjacentCells.filter((c) => a[c.x][c.y] == v);
  }

  /**
   * Finds the cells which are directly adjacent, orthogonally and diagonally to
   * a set of other cells, that have a given value.
   * @param a 2D array containing the values of each cell in the area.
   * @param c Array of cells of which to find adjacent cells with a certain
   * value.
   * @param v Value of which to find adjacent cells with.
   * @returns Array of cells adjacent to cells in c that have value v.
   */
  static getAdjacentCellsOfCellsWithValue<Type>(
    a: Type[][],
    c: Cell[],
    v: Type
  ): Cell[] {
    let adjacentCellsWithValue: Cell[] = [];

    // Loop through cells in c.
    c.forEach((cell) => {
      // Find cells adjacent to current cell with value.
      let currentAdjacentCellsWithValue = this.getAdjacentCellsOfCellWithValue(
        a,
        cell,
        v
      );

      // Add adjacent cells to return value if not already added.
      currentAdjacentCellsWithValue.forEach((cacwv) => {
        if (
          !adjacentCellsWithValue.find(
            (acwv) => acwv.x == cacwv.x && acwv.y == cacwv.y
          )
        ) {
          adjacentCellsWithValue.push(new Cell(cacwv.x, cacwv.y));
        }
      });
    });

    // Return array of cells.
    return adjacentCellsWithValue;
  }

  /**
   * Tests whether a cell is in a certain area.
   * @param w Width of the area containing the cells.
   * @param h Height of the area containing the cells.
   * @param c Cell in question.
   * @returns True if cell is within area, false otherwise.
   */
  static withinArea(w: number, h: number, c: Cell): boolean {
    return c.x >= 0 && c.x < w && c.y >= 0 && c.y < h ? true : false;
  }

  /**
   * Tests whether a cell is on the perimeter of a certain area.
   * @param w Width of the area containing the cells.
   * @param h Height of the area containing the cells.
   * @param c Cell in question.
   * @returns True if cell is on the perimeter of the area, false otherwise.
   */
  static onPerimeter(w: number, h: number, c: Cell): boolean {
    return this.withinArea(w, h, c) &&
      (c.x == 0 || c.x == w - 1 || c.y == 0 || c.y == h - 1)
      ? true
      : false;
  }
}
