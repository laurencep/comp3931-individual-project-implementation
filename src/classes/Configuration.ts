import * as _ from "lodash";
import Event from "./Event";
import Cell from "./Cell";
import { ConfigurationSnapshot } from "./Snapshots";

/**
 * Represents a configuration of a room including its dimensions, doors,
 * obstacles and pedestrians.
 */
export default class Configuration {
  // Diagonal distance between cells.
  static readonly LAMBDA = 1.5;

  // Width of the room.
  private _width: number = 20;

  // Height of the room.
  private _height: number = 16;

  // Array of cells representing locations of doors in the room.
  private _doors: Cell[] = [new Cell(0, 7)];

  // Array of cells representing locations of obstacles in the room.
  private _obstacles: Cell[] = [new Cell(2, 6), new Cell(2, 7), new Cell(2, 8)];

  // Array of cells representing locations of pedestrians in the room.
  private _pedestrians: Cell[] = [new Cell(15, 7)];

  // Probability that a pedestrian will not move in a certain time step.
  private _panic: number = 0.05;

  // Event occuring when the configuration is changed.
  private _modified: Event = new Event();

  /**
   * Creates a new configuration from a given configuration snapshot.
   * @param configuration Configuration snapshot from which to create a
   * configuration.
   * @returns Configuration that the given snapshot represents.
   */
  static import(configuration: ConfigurationSnapshot): Configuration {
    // If the configuration snapshot is valid, create a new configuration, throw
    // error if not.
    if (this.isValid(configuration)) {
      let c = new Configuration();

      c._width = configuration.width;
      c._height = configuration.height;
      c._doors = configuration.doors;
      c._obstacles = configuration.obstacles;
      c._pedestrians = configuration.pedestrians;
      c._panic = configuration.panic;

      return c;
    } else {
      throw new Error("Invalid configuration.");
    }
  }

  /**
   * Creates a new configuration with default parameters:
   * A 20x16 room with a door at (0, 7), obstacles at (2, 6), (2, 7) and (2, 8),
   * a pedestrian at (15, 7) and a panic probability of 0.05.
   */
  constructor() {}

  /**
   * Width of the room.
   */
  get width() {
    return this._width;
  }

  /**
   * Height of the room.
   */
  get height() {
    return this._height;
  }

  /**
   * Array of cells representing locations of doors in the room.
   */
  get doors() {
    return this._doors;
  }

  /**
   * Array of cells representing locations of obstacles in the room.
   */
  get obstacles() {
    return this._obstacles;
  }

  /**
   * Array of cells representing locations of pedestrians in the room.
   */
  get pedestrians() {
    return this._pedestrians;
  }

  /**
   * Probability that a pedestrian will not move in a certain time step.
   */
  get panic() {
    return this._panic;
  }

  /**
   * Event occuring when the configuration is changed.
   */
  get modified() {
    return this._modified.expose();
  }

  /**
   * Can be set between 0 and 100, error thrown if value outside that range.
   */
  set panic(p: number) {
    // Check that p is inside valid range, throw error if not.
    if (p >= 0 && p <= 100) {
      // Set panic value and trigger modified event.
      this._panic = p / 100;
      this._modified.trigger();
    } else {
      throw new RangeError("Panic must be between 0 and 100.");
    }
  }

  /**
   * Places a door in a certain cell, removes a door if already present.
   * @param c Cell in which to toggle a door.
   */
  toggleDoor(c: Cell): void {
    // If cell is on perimeter of room, toggle a door.
    if (Cell.onPerimeter(this._width, this._height, c)) {
      // If not already a door, add it. If already a door, remove it.
      if (_.find(this._doors, c) == undefined) {
        this._doors.push(c);
      } else {
        _.remove(this._doors, c);
      }

      // Trigger modified event.
      this._modified.trigger();
    }
  }

  /**
   * Places an obstacle in a certain cell, removes an obstacle if already
   * present.
   * @param c Cell in which to toggle an obstacle.
   */
  toggleObstacle(c: Cell): void {
    // If cell is within the room, not on perimeter and not a pedestrian,
    // toggle an obstacle.
    if (
      Cell.withinArea(this._width, this._height, c) &&
      !Cell.onPerimeter(this._width, this._height, c) &&
      _.find(this._pedestrians, c) == undefined
    ) {
      // If not already an obstacle, add it. If already an obstacle, remove it.
      if (_.find(this._obstacles, c) == undefined) {
        this._obstacles.push(c);
      } else {
        _.remove(this._obstacles, c);
      }

      // Trigger modified event.
      this._modified.trigger();
    }
  }

  /**
   * Places a pedestrian in a certain cell, removes a pedestrian if alraedy
   * present.
   * @param c Cell in which to toggle a pedestrian.
   */
  togglePedestrian(c: Cell): void {
    // If cell is within the area, not on the perimeter and not an obstacle,
    // toggle a pedestrian.
    if (
      Cell.withinArea(this._width, this._height, c) &&
      !Cell.onPerimeter(this._width, this._height, c) &&
      _.find(this._obstacles, c) == undefined
    ) {
      // If not already a pedestrian, add it. If already a pedestrian, remove
      // it.
      if (_.find(this._pedestrians, c) == undefined) {
        this._pedestrians.push(c);
      } else {
        _.remove(this._pedestrians, c);
      }

      // Trigger modified event.
      this._modified.trigger();
    }
  }

  /**
   * Removes all doors from the configuration.
   */
  clearDoors(): void {
    // Empty door array and trigger modified event.
    this._doors = [];
    this._modified.trigger();
  }

  /**
   * Removes all obstacles from the configuration.
   */
  clearObstacles(): void {
    // Empty obstacle array and trigger modified event.
    this._obstacles = [];
    this._modified.trigger();
  }

  /**
   * Removes all pedestrians from the configuration.
   */
  clearPedestrians(): void {
    // Empty pedestrian array and trigger modified event.
    this._pedestrians = [];
    this._modified.trigger();
  }

  /**
   * Places a given number of pedestrians at random locations in the room.
   * @param n Number of pedestrians to place.
   */
  placeRandomPedestrians(n: number): void {
    // Get the empty cells in the room.
    let emptyCells = this.getEmptyCells();

    // If n is greater than the number of empty cells, make n the number of
    // empty cells.
    n = n <= emptyCells.length ? n : emptyCells.length;

    // Select n random cells from the empty cells.
    let cellsToFill = _.sampleSize(emptyCells, n);

    // Place pedestrians in the randomly selected cells.
    cellsToFill.forEach((c) => {
      this._pedestrians.push(c);
    });

    // Trigger modified event.
    this._modified.trigger();
  }

  /**
   * Modifies the dimensions of the room, removing pedestrians and obstacles
   * where necessary.
   * @param x New width of the room.
   * @param y New height of the room.
   */
  changeDimensions(x: number, y: number): void {
    // Throw error if dimensions too small
    if (x < 3 || y < 3) {
      throw new RangeError("Dimensions cannot be smaller than 3x3.");
    }

    // Update width and height.
    this._width = x;
    this._height = y;

    // Remove any doors that are no longer on perimeter.
    this._doors = this._doors.filter((door) =>
      Cell.onPerimeter(this._width, this._height, door)
    );

    // Remove any obstacles that are no loner within the area and not on the
    // perimeter.
    this._obstacles = this._obstacles.filter(
      (obstacle) =>
        Cell.withinArea(this._width, this._height, obstacle) &&
        !Cell.onPerimeter(this._width, this._height, obstacle)
    );

    // Remove any pedestrians that are no longer within the area and not on the
    // perimeter.
    this._pedestrians = this._pedestrians.filter(
      (pedestrian) =>
        Cell.withinArea(this._width, this._height, pedestrian) &&
        !Cell.onPerimeter(this._width, this._height, pedestrian)
    );

    // Trigger modified event.
    this._modified.trigger();
  }

  /**
   * Creates a snapshot representing the current state of the configuration.
   * @returns Configuration snapshot representing the current state of this
   * configuration.
   */
  snapshot(): ConfigurationSnapshot {
    return {
      width: this._width,
      height: this._height,
      doors: _.cloneDeep(this._doors),
      obstacles: _.cloneDeep(this._obstacles),
      pedestrians: _.cloneDeep(this._pedestrians),
      panic: this._panic,
    };
  }

  /**
   * Generates an array of cells in the room that are empty.
   * @returns Array of cells in the room that are empty.
   */
  private getEmptyCells(): Cell[] {
    let emptyCells: Cell[] = [];

    // For each cell in the room, check if it is empty.
    for (let i = 0; i < this._width; ++i) {
      for (let j = 0; j < this._height; ++j) {
        let c = new Cell(i, j);

        // If cell is not a pedestrian, is not on the perimeter and is not an
        // obstacle, add it to the return value.
        if (
          _.find(this._pedestrians, c) == undefined &&
          !Cell.onPerimeter(this._width, this._height, c) &&
          _.find(this._obstacles, c) == undefined
        ) {
          emptyCells.push(c);
        }
      }
    }

    // Return array of empty cells.
    return emptyCells;
  }

  /**
   * Tests whether a given configuration snapshot is valid.
   * @param c Configuration snapshot for which to check the validity.
   * @returns True if c is a valid snapshot, false otherwise.
   */
  private static isValid(c: ConfigurationSnapshot): boolean {
    try {
      // Check that dimensions are not too small.
      if (c.width < 3 || c.height < 3) {
        return false;
      }

      // Check there are no duplicate doors.
      if (_.uniqWith(c.doors, _.isEqual).length < c.doors.length) {
        return false;
      }

      // Check that all doors are on the perimeter.
      if (c.doors.some((door) => !Cell.onPerimeter(c.width, c.height, door))) {
        return false;
      }

      // Check there are no duplicate obstacles.
      if (_.uniqWith(c.obstacles, _.isEqual).length < c.obstacles.length) {
        return false;
      }

      // Check that all obstacles are within the area and not on the perimeter.
      if (
        c.obstacles.some(
          (obstacle) =>
            !(
              Cell.withinArea(c.width, c.height, obstacle) &&
              !Cell.onPerimeter(c.width, c.height, obstacle)
            )
        )
      ) {
        return false;
      }

      // Check there are no duplicate pedestrians.
      if (_.uniqWith(c.pedestrians, _.isEqual).length < c.pedestrians.length) {
        return false;
      }

      // Check that all pedestrians are within the area and not on the
      // perimeter.
      if (
        c.pedestrians.some(
          (pedestrian) =>
            !(
              Cell.withinArea(c.width, c.height, pedestrian) &&
              !Cell.onPerimeter(c.width, c.height, pedestrian)
            )
        )
      ) {
        return false;
      }

      // Check that no pedestrians and obstacles overlap.
      if (
        c.pedestrians.some(
          (pedestrian) => _.find(c.obstacles, pedestrian) != undefined
        )
      ) {
        return false;
      }

      // Check that the panic value is in the valid range.
      if (c.panic < 0 || c.panic > 1) {
        return false;
      }

      // Return true if no issues found.
      return true;

      // Catch any undefined properties.
    } catch (e) {
      return false;
    }
  }
}
