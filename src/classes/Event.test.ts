import Event, { IEvent } from "./Event";

describe("subscribe", () => {
  test("adds method to handlers run when trigger is called", () => {
    let handler = jest.fn();

    let e = new Event();

    e.subscribe(handler);

    e.trigger();

    expect(handler).toBeCalled();
  });

  test("allows multiple methods to subscribe", () => {
    let handler1 = jest.fn();
    let handler2 = jest.fn();

    let e = new Event();

    e.subscribe(handler1);
    e.subscribe(handler2);

    e.trigger();

    expect(handler1).toBeCalled();
    expect(handler2).toBeCalled();
  });
});

describe("unsubscribe", () => {
  test("removes methods from handlers", () => {
    let handler = jest.fn();

    let e = new Event();

    e.subscribe(handler);

    e.unsubscribe(handler);

    e.trigger();

    expect(handler).not.toBeCalled();
  });

  test("only removes given method", () => {
    let handler1 = jest.fn();
    let handler2 = jest.fn();

    let e = new Event();

    e.subscribe(handler1);
    e.subscribe(handler2);

    e.unsubscribe(handler1);

    e.trigger();

    expect(handler1).not.toBeCalled();
    expect(handler2).toBeCalled();
  });
});

describe("trigger", () => {
  test("calls all handlers", () => {
    let handler1 = jest.fn();
    let handler2 = jest.fn();

    let e = new Event();

    e.subscribe(handler1);
    e.subscribe(handler2);

    e.trigger();

    expect(handler1).toBeCalled();
    expect(handler2).toBeCalled();
  });
});
