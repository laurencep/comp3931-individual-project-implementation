import Cell from "./Cell";
import Configuration from "./Configuration";
import Event from "./Event";
import Simulation from "./Simulation";
import { SimulationState, State } from "./States";

describe("constructor", () => {
  test("succeeds when given a valid configuration", () => {
    expect(() => new Simulation(new Configuration())).not.toThrow();
  });
});

describe("state", () => {
  test("returns the state of the configuration", () => {
    expect(new Simulation(new Configuration()).state).toEqual(
      State.Initialised || State.Running || State.Paused || State.Finished
    );
  });
});

describe("configuration", () => {
  test("returns the configuration of the simulation", () => {
    expect(
      new Simulation(new Configuration()).configuration instanceof Configuration
    ).toBe(true);
  });
});

describe("pedestrians", () => {
  test("returns an array of the current pedestrian locations", () => {
    expect(
      new Simulation(new Configuration()).pedestrians.every(
        (pedestrian) => pedestrian instanceof Cell
      )
    ).toBe(true);
  });
});

describe("time", () => {
  test("returns the current time of the simulation", () => {
    expect(typeof new Simulation(new Configuration()).time).toBe("number");
  });
});

describe("pedestrianCountAtTime", () => {
  test("returns array containing pedestrian count at different times", () => {
    expect(
      new Simulation(new Configuration()).pedestrianCountAtTime.every(
        (entry) => typeof entry == "number"
      )
    ).toBe(true);
  });
});

describe("interval", () => {
  test("returns the interval", () => {
    expect(typeof new Simulation(new Configuration()).interval).toBe("number");
  });

  test("can be set to a positive value", () => {
    let s = new Simulation(new Configuration());
    s.interval = 10;
    expect(s.interval).toBe(10);
  });

  test("throws error if set to a negative value", () => {
    expect(() => (new Simulation(new Configuration()).interval = -1)).toThrow();
  });

  test("triggers updated event when changed", () => {
    let s = new Simulation(new Configuration());
    let handler = jest.fn();
    s.updated.subscribe(handler);
    s.interval = 100;
    expect(handler).toBeCalled();
  });
});

describe("updated", () => {
  test("returns the updated event", () => {
    expect(new Simulation(new Configuration()).updated instanceof Event).toBe(
      true
    );
  });
});

describe("play", () => {
  test("sets the state to running", () => {
    let s = new Simulation(new Configuration());
    s.play();
    expect(s.state).toBe(State.Running);
    s.pause();
  });

  test("performs initial progression", () => {
    let c = Configuration.import({
      width: 5,
      height: 5,
      doors: [new Cell(0, 2)],
      obstacles: [new Cell(2, 2)],
      pedestrians: [new Cell(3, 1), new Cell(3, 3)],
      panic: 0,
    });
    let s = new Simulation(c);
    s.interval = 10000;
    s.play();
    s.pause();
    expect(s.pedestrians).toEqual([new Cell(2, 1), new Cell(2, 3)]);
  });

  test("triggers updated event", () => {
    let c = new Simulation(new Configuration());
    let handler = jest.fn();
    c.updated.subscribe(handler);
    c.play();
    c.pause();
    expect(handler).toBeCalled();
  });
});

describe("step", () => {
  test("sets the state to pause", () => {
    let s = new Simulation(new Configuration());
    s.step();
    expect(s.state).toBe(State.Paused);
  });

  test("performs progression", () => {
    let c = Configuration.import({
      width: 5,
      height: 5,
      doors: [new Cell(0, 2)],
      obstacles: [new Cell(2, 2)],
      pedestrians: [new Cell(3, 1), new Cell(3, 3)],
      panic: 0,
    });
    let s = new Simulation(c);
    s.step();
    expect(s.pedestrians).toEqual([new Cell(2, 1), new Cell(2, 3)]);
  });

  test("triggers updated event", () => {
    let c = new Simulation(new Configuration());
    let handler = jest.fn();
    c.updated.subscribe(handler);
    c.step();
    expect(handler).toBeCalled();
  });
});

describe("pause", () => {
  test("sets the state to pause", () => {
    let s = new Simulation(new Configuration());
    s.pause();
    expect(s.state).toBe(State.Paused);
  });

  test("triggers updated event", () => {
    let c = new Simulation(new Configuration());
    let handler = jest.fn();
    c.updated.subscribe(handler);
    c.pause();
    expect(handler).toBeCalled();
  });
});

describe("complete", () => {
  test("sets the state to finished", () => {
    let s = new Simulation(new Configuration());
    s.complete();
    expect(s.state).toBe(State.Finished);
  });

  test("runs a simulation to completion", () => {
    let c = new Configuration();
    c.placeRandomPedestrians(10);
    let s = new Simulation(c);
    s.complete();
    expect(s.pedestrians.length).toBe(0);
  });
});
