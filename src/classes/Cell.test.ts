import Cell from "./Cell";
import Point from "./Point";

describe("constructor", () => {
  test("succeeds when given integer coordinates", () => {
    expect(() => {
      new Cell(5, 7);
    }).not.toThrow();
  });

  test("throws when one coordinate is not an integer", () => {
    expect(() => {
      new Cell(2, 3.6);
    }).toThrow();
  });

  test("throws when both coordinates are not an integer", () => {
    expect(() => {
      new Cell(2.3, 5.7);
    }).toThrow();
  });
});

describe("fromPoint", () => {
  test("creates correct cell with correct x coordinate given point", () => {
    expect(Cell.fromPoint(new Point(60.5, 70.99), 10).x).toBe(6);
  });

  test("creates correct cell with correct y coordinate given point", () => {
    expect(Cell.fromPoint(new Point(60.5, 70.99), 10).y).toBe(7);
  });
});

describe("manhattanDistance", () => {
  test("returns correct value for two orthogonally adjacent cells", () => {
    expect(Cell.manhattanDistance(new Cell(5, 5), new Cell(5, 6))).toBe(1);
  });

  test("returns correct value for two diagonally adjacent cells", () => {
    expect(Cell.manhattanDistance(new Cell(5, 5), new Cell(6, 6))).toBe(2);
  });

  test("returns correct value for two far apart cells", () => {
    expect(Cell.manhattanDistance(new Cell(5, 5), new Cell(8, 12))).toBe(10);
  });
});

describe("getAdjacentCellsOfCell", () => {
  test("returns correct cells for cell in middle of area", () => {
    expect(Cell.getAdjacentCellsOfCell(10, 10, new Cell(5, 5))).toEqual([
      new Cell(5, 4),
      new Cell(6, 4),
      new Cell(6, 5),
      new Cell(6, 6),
      new Cell(5, 6),
      new Cell(4, 6),
      new Cell(4, 5),
      new Cell(4, 4),
    ]);
  });

  test("returns correct cells for cell on edge of area", () => {
    expect(Cell.getAdjacentCellsOfCell(10, 10, new Cell(0, 5))).toEqual([
      new Cell(0, 4),
      new Cell(1, 4),
      new Cell(1, 5),
      new Cell(1, 6),
      new Cell(0, 6),
    ]);
  });

  test("returns correct cells for cell in corner of area", () => {
    expect(Cell.getAdjacentCellsOfCell(10, 10, new Cell(0, 0))).toEqual([
      new Cell(1, 0),
      new Cell(1, 1),
      new Cell(0, 1),
    ]);
  });
});

describe("getAdjacentCellsOfCellWithValue", () => {
  test("returns all adjacent cells when values match", () => {
    expect(
      Cell.getAdjacentCellsOfCellWithValue(
        [
          [true, true, true],
          [true, true, true],
          [true, true, true],
        ],
        new Cell(1, 1),
        true
      ).sort((a, b) => a.x - b.x || b.x - b.y)
    ).toEqual([
      new Cell(0, 0),
      new Cell(0, 1),
      new Cell(0, 2),
      new Cell(1, 0),
      new Cell(1, 2),
      new Cell(2, 0),
      new Cell(2, 1),
      new Cell(2, 2),
    ]);
  });

  test("returns correct cells when some values match", () => {
    expect(
      Cell.getAdjacentCellsOfCellWithValue(
        [
          [true, false, false],
          [false, true, true],
          [true, false, true],
        ],
        new Cell(1, 1),
        true
      ).sort((a, b) => a.x - b.x || b.x - b.y)
    ).toEqual([new Cell(0, 0), new Cell(1, 2), new Cell(2, 0), new Cell(2, 2)]);
  });

  test("returns no cells when no values match", () => {
    expect(
      Cell.getAdjacentCellsOfCellWithValue(
        [
          [true, true, true],
          [true, true, true],
          [true, true, true],
        ],
        new Cell(1, 1),
        false
      )
    ).toEqual([]);
  });
});

describe("getAdjacentCellsOfCellsWithValue", () => {
  test("returns correct cells when adjacent cells do not overlap", () => {
    expect(
      Cell.getAdjacentCellsOfCellsWithValue(
        [
          [true, true, false, true],
          [false, true, true, false],
          [false, true, false, true],
          [false, true, true, true],
        ],
        [new Cell(0, 0), new Cell(3, 3)],
        true
      ).sort((a, b) => a.x - b.x || b.x - b.y)
    ).toEqual([new Cell(0, 1), new Cell(1, 1), new Cell(2, 3), new Cell(3, 2)]);
  });

  test("returns correct cells when adjacent cells overlap", () => {
    expect(
      Cell.getAdjacentCellsOfCellsWithValue(
        [
          [true, true, false, true],
          [false, true, true, false],
          [false, true, false, true],
          [false, true, true, true],
        ],
        [new Cell(1, 1), new Cell(2, 2)],
        true
      ).sort((a, b) => a.x - b.x || b.x - b.y)
    ).toEqual([
      new Cell(0, 0),
      new Cell(0, 1),
      new Cell(1, 1),
      new Cell(1, 2),
      new Cell(2, 1),
      new Cell(2, 3),
      new Cell(3, 1),
      new Cell(3, 2),
      new Cell(3, 3),
    ]);
  });
});

describe("withinArea", () => {
  test("returns true if cell is in middle of the area", () => {
    expect(Cell.withinArea(5, 5, new Cell(3, 2))).toBe(true);
  });

  test("returns true if the cell is on the edge of the area", () => {
    expect(Cell.withinArea(5, 5, new Cell(0, 2))).toBe(true);
  });

  test("returns true if cell is in corner of the area", () => {
    expect(Cell.withinArea(5, 5, new Cell(0, 0))).toBe(true);
  });

  test("returns false if cell is outside area", () => {
    expect(Cell.withinArea(5, 5, new Cell(5, 0))).toBe(false);
  });
});

describe("onPerimeter", () => {
  test("returns true if cell is in middle of edge", () => {
    expect(Cell.onPerimeter(5, 5, new Cell(0, 2))).toBe(true);
  });

  test("returns true if cell is in corner of the area", () => {
    expect(Cell.onPerimeter(5, 5, new Cell(0, 0))).toBe(true);
  });

  test("returns false if cell is in middle of the area", () => {
    expect(Cell.onPerimeter(5, 5, new Cell(3, 2))).toBe(false);
  });

  test("returns false if cell is outside area", () => {
    expect(Cell.onPerimeter(5, 5, new Cell(5, 0))).toBe(false);
  });
});
