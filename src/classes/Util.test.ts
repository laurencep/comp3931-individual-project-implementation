import { init2DArray } from "./Util";

describe("init2DArray", () => {
  test("sets dimension 1 size correctly", () => {
    expect(init2DArray<number>(5, 10, 3).length).toBe(5);
  });

  test("sets dimension 2 size correctly", () => {
    expect(init2DArray<number>(5, 10, 3)[0].length).toBe(10);
  });

  test("returns values correctly", () => {
    expect(
      init2DArray<number>(5, 10, 3)
        .flat()
        .every((v) => v == 3)
    ).toBe(true);
  });
});
