/**
 * Creates a 2D array of a given size and initialises each entry to a given
 * value.
 * @param l1 Length of the first array dimension.
 * @param l2 Length of the second arary dimension.
 * @param v Initial value of each entry in the array.
 * @returns 2D array of size l1 x l2 with each entry of value v.
 */
export function init2DArray<Type>(l1: number, l2: number, v: Type) {
  let a: Type[][] = [];
  for (let i = 0; i < l1; ++i) {
    a[i] = [];
    for (let j = 0; j < l2; ++j) {
      a[i][j] = v;
    }
  }
  return a;
}
