import Cell from "./Cell";

/**
 * Represents a single movement of a single pedestrian.
 */
export default class Move {
  private readonly _current: Cell; // Cell the pedestrian is moving from.
  private readonly _next: Cell; // Cell the pedestrian is moving to.

  /**
   * Creates a new move from one given cell to another.
   * @param current Cell the pedestrian is moving from.
   * @param next Cell the pedestrian is moving to.
   */
  constructor(current: Cell, next: Cell) {
    this._current = current;
    this._next = next;
  }

  /**
   * Cell the pedestrian is moving from.
   */
  get current() {
    return this._current;
  }

  /**
   * Cell the pedestrian is moving to.
   */
  get next() {
    return this._next;
  }
}
