import Configuration from "./Configuration";
import Simulation from "./Simulation";

/**
 * Represents a series of simulations using the same configuration.
 */
export default class SimulationSeries {
  // Configuration to use for the simulations.
  private _configuration: Configuration;

  // Number of simulations to run.
  private _toRun: number;

  // Number of simulations that have been run.
  private _noRun: number = 0;

  // Average time taken for the simulations to run.
  private _averageTime: number | null = null;

  // True if the simulation series has begun simulating, false otherwise.
  private _started: boolean = false;

  // 2D array of the number of pedestrians in the room at each time step for
  // each simulation in the series.
  private _pedestrianCountsAtTime: number[][] = [];

  /**
   * Creates a simulation series for a given number of simulations for a given
   * configuration.
   * @param configuration Configuration to use for the simulations.
   * @param toRun Number of simulations to run.
   */
  constructor(configuration: Configuration, toRun: number) {
    this._configuration = configuration;
    this._toRun = toRun;
  }

  /**
   * Average time taken for the simulations to run.
   */
  get averageTime() {
    return this._averageTime;
  }

  /**
   * 2D array of the number of pedestrians in the room at each time step for
   * each simulation in the series.
   */
  get pedestrianCountsAtTime() {
    return this._pedestrianCountsAtTime;
  }

  /**
   * Completes the simulations and calculates an average evacuation time.
   */
  run(): void {
    // Throw an error if the simulation series has already been started.
    if (this._started) {
      throw new Error("Series already run.");
    } else {
      this._started = true;
    }

    let currentSimulation: Simulation;
    let totalTime = 0;

    // Run the correct number of simulations.
    while (this._noRun < this._toRun) {
      // Create and complete new simulation.
      currentSimulation = new Simulation(this._configuration);
      currentSimulation.complete();

      // Add evacuation time to total time.
      totalTime += currentSimulation.time;

      // Add the count of pedestrians at time steps to the series record.
      this.pedestrianCountsAtTime.push(currentSimulation.pedestrianCountAtTime);

      // Increment run number.
      this._noRun++;
    }

    // Calculate average evacuation time.
    this._averageTime = totalTime / this._noRun;
  }
}
