import Point from "./Point";

describe("constructor", () => {
  test("sets x coordinate correctly", () => {
    expect(new Point(2, 3).x).toBe(2);
  });

  test("sets y coordinate correctly", () => {
    expect(new Point(2, 3).y).toBe(3);
  });
});
