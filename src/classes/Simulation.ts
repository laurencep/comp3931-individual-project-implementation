import * as _ from "lodash";
import Cell from "./Cell";
import Move from "./Move";
import Event from "./Event";
import Configuration from "./Configuration";
import { init2DArray } from "./Util";
import { SimulationState, State } from "./States";

/**
 * Floor field values for different objects in a room.
 */
const enum FloorField {
  Wall = 500,
  Door = 1,
}

/**
 * An error that occurs when a pedestrian has no route to an exit.
 */
export class TrappedPedestrianError extends Error {
  constructor(message: string) {
    super(message);
  }
}

/**
 * Represents a single simulation run on a configuration.
 */
export default class Simulation {
  // Current state of the simulation.
  private _state: SimulationState = State.Initialised;

  // Configuration of the room the simulation is using.
  private _configuration: Configuration;

  // Array of values representing the shortest distance from each cell to a
  // door.
  private _floorField: number[][] = init2DArray<number>(0, 0, 0);

  // Array of current locations of pedestrians in the room.
  private _pedestrians: Cell[];

  // Current time of the simulation.
  private _time: number = 0;

  // Record of number of pedestrians in the room at each time step.
  private _pedestrianCountAtTime: number[] = [];

  // Milliseconds to wait in between each simulation step when playing.
  private _interval: number = 1000;

  // Handle for the timer when the simulation is playing.
  private _timerHandle: NodeJS.Timer | null = null;

  // Event occuring when the simulation is updated.
  private _updated: Event = new Event();

  /**
   * Creates a new simulation from the given configuration.
   * @param configuration Configuration to create a simulation for.
   */
  constructor(configuration: Configuration) {
    // Set the configuration.
    this._configuration = _.cloneDeep(configuration);

    // Calculate the floor field.
    this.calculateFloorField();

    // Check if all pedestrians can evacuate, throw error if not.
    if (
      this._configuration.pedestrians.some(
        (pedestrian) => this._floorField[pedestrian.x][pedestrian.y] == 0
      )
    ) {
      throw new TrappedPedestrianError(
        "Cannot play while pedestrians are trapped."
      );
    }

    // Set the initial locations of the pedestrians.
    this._pedestrians = _.cloneDeep(this._configuration.pedestrians);

    // Add the number of pedestrians at time 0 to the record.
    this._pedestrianCountAtTime.push(this._pedestrians.length);
  }

  /**
   * Current state of the simulation.
   */
  get state() {
    return this._state;
  }

  /**
   * Configuration of the room the simulation is using.
   */
  get configuration() {
    return this._configuration;
  }

  /**
   * Array of current locations of pedestrians in the room.
   */
  get pedestrians() {
    return this._pedestrians;
  }

  /**
   * Current time of the simulation.
   */
  get time() {
    return this._time;
  }

  /**
   * Record of number of pedestrians in the room at each time step.
   */
  get pedestrianCountAtTime() {
    return this._pedestrianCountAtTime;
  }

  /**
   * Milliseconds to wait in between each simulation step when playing.
   */
  get interval() {
    return this._interval;
  }

  /**
   * Event occuring when the simulation is updated.
   */
  get updated() {
    return this._updated.expose();
  }

  /**
   * Can be set to any positive number.
   */
  set interval(i: number) {
    // If number is not positive, throw error.
    if (i >= 0) {
      // Set interval.
      this._interval = i;

      // If timer currently running, clear it and create new timer with new
      // interval.
      if (this._timerHandle != null) {
        clearInterval(this._timerHandle);
        this._timerHandle = setInterval(
          this.progressAndUpdate.bind(this),
          this._interval
        );
      }

      // Trigger updated event.
      this._updated.trigger();
    } else {
      throw new RangeError("Interval must be positive.");
    }
  }

  /**
   * Begins running the simulation.
   */
  play(): void {
    // Update state.
    this._state = State.Running;

    // Create timer to progess simulation after each interval.
    this._timerHandle = setInterval(
      this.progressAndUpdate.bind(this),
      this._interval
    );

    // Perform initial progression and trigger update.
    this.progressAndUpdate();
  }

  /**
   * Progresses the simulation by a single step.
   */
  step(): void {
    // Update state.
    this._state = State.Paused;

    // Perform progression and trigger update.
    this.progressAndUpdate();
  }

  /**
   * Pauses the simulation if it is currently running.
   */
  pause(): void {
    // Clear timer if currently active.
    if (this._timerHandle != null) {
      clearInterval(this._timerHandle);
      this._timerHandle = null;
    }

    // Update state.
    this._state = State.Paused;

    // Trigger updated event.
    this._updated.trigger();
  }

  /**
   * Runs the simulation until completion.
   */
  complete(): void {
    // Progress the simulation until it is finished.
    while ((this._state as SimulationState) != State.Finished) {
      this.progress();
    }

    // Trigger updated event.
    this._updated.trigger();
  }

  /**
   * Performs a progression of the simulation and triggers the updated event.
   */
  private progressAndUpdate(): void {
    // Progress simulation.
    this.progress();

    // Trigger updated event.
    this._updated.trigger();
  }

  /**
   * Progresses the simulation by one step.
   */
  private progress(): void {
    // Create arrays to hold intended and actual moves.
    let intendedMoves: Move[] = [];
    let actualMoves: Move[] = [];

    // Increment time counter.
    ++this._time;

    // If simulation finished clear timer, update state and return.
    if (this._pedestrians.length == 0) {
      // If timer is active, clear it.
      if (this._timerHandle != null) {
        clearInterval(this._timerHandle);
        this._timerHandle = null;
      }

      // Add 0 pedestrians at current time to record.
      this._pedestrianCountAtTime.push(0);

      // Update state.
      this._state = State.Finished;

      // Return to caller.
      return;
    }

    // For each pedestrian, calculate intended move.
    this._pedestrians.forEach((pedestrian) => {
      // If pedestrian is panicking, don't move.
      if (Math.random() < this._configuration.panic) {
        return;
      }

      // If pedestrian is on a door, don't need to calculate the move as they
      // just exit.
      if (_.find(this._configuration.doors, pedestrian)) {
        return;
      }

      // Get possible cells to move to.
      let emptyAdjacentCells = Cell.getAdjacentCellsOfCell(
        this._configuration.width,
        this._configuration.height,
        pedestrian
      ).filter((c) => _.find(this._pedestrians, c) == undefined);

      // If no cells to move to, don't move.
      if (emptyAdjacentCells.length == 0) {
        return;
      }

      // Get floor field value for each possible move.
      let possibleMoves = emptyAdjacentCells.map((c) => {
        return {
          c: c,
          ffv: this._floorField[c.x][c.y],
        };
      });

      // Sort by floor field asc.
      possibleMoves = possibleMoves.sort((a, b) => a.ffv - b.ffv);

      // Filter for minimum floor field, could be more than one.
      possibleMoves = possibleMoves.filter(
        (x) => x.ffv == possibleMoves[0].ffv
      );

      // If all possible moves have equal or greater values to the current cell,
      // don't move.
      if (
        possibleMoves[0].ffv >= this._floorField[pedestrian.x][pedestrian.y]
      ) {
        return;
      }

      // Pick a random move.
      let intendedMove =
        possibleMoves[Math.floor(Math.random() * possibleMoves.length)];

      // Add move to intended moves array.
      intendedMoves.push(
        new Move(pedestrian, new Cell(intendedMove.c.x, intendedMove.c.y))
      );
    });

    // Extract destinations from intended moves.
    let intendedDestinations = intendedMoves.map((im) => im.next);

    // Remove duplicates.
    let uniqueIntendedDestinations = intendedDestinations.filter(
      (uid, i, self) =>
        i == self.findIndex((uid_) => uid.x == uid_.x && uid.y == uid_.y)
    );

    // For each unique intended destination, randomly pick a move that will be
    // executed.
    uniqueIntendedDestinations.forEach((uid) => {
      let movesWithDestination = intendedMoves.filter(
        (im) => im.next.x == uid.x && im.next.y == uid.y
      );

      actualMoves.push(
        movesWithDestination[
          Math.floor(Math.random() * movesWithDestination.length)
        ]
      );
    });

    // Make pedestrians standing on a door cell exit the room.
    this._configuration.doors.forEach((door) => {
      _.remove(this._pedestrians, door);
    });

    // Execute moves by updating pedestrian array.
    actualMoves.forEach((move) => {
      _.remove(this._pedestrians, move.current);
      this._pedestrians.push(move.next);
    });

    // Add number of pedestrians at current time step to record.
    this._pedestrianCountAtTime.push(this._pedestrians.length);
  }

  /**
   * Calculates the floor field of the simulation's configuration.
   */
  private calculateFloorField(): void {
    // Create empty array for floor field.
    this._floorField = init2DArray<number>(
      this._configuration.width,
      this._configuration.height,
      0
    );

    // Fill in edges of room as walls.
    for (let i = 0; i < this._configuration.height; ++i) {
      this._floorField[0][i] = FloorField.Wall;
      this._floorField[this._configuration.width - 1][i] = FloorField.Wall;
    }
    for (let i = 1; i < this._configuration.width - 1; ++i) {
      this._floorField[i][0] = FloorField.Wall;
      this._floorField[i][this._configuration.height - 1] = FloorField.Wall;
    }

    // Fill in the doors.
    this._configuration.doors.forEach((door) => {
      this._floorField[door.x][door.y] = FloorField.Door;
    });

    // Fill in obstacles.
    this._configuration.obstacles.forEach((obstacle) => {
      this._floorField[obstacle.x][obstacle.y] = FloorField.Wall;
    });

    // Get the cells adjacent to the doors to start incrementally calculating
    // the floor field.
    let previousCells: Cell[] = _.cloneDeep(this._configuration.doors);
    let currentCells: Cell[] = Cell.getAdjacentCellsOfCellsWithValue(
      this._floorField,
      previousCells,
      0
    );

    // Incrementally calculate floor field until all cells have a value.
    while (currentCells.length != 0) {
      // Find lowest possible value for each of the current cells and assign it
      // to the floor field.
      currentCells.forEach((currentCell) => {
        // Get the adjacent cell with the lowest assigned floor field value.
        let currentLowestValuedCell =
          this.getLowestFloorFieldAdjacentCell(currentCell);

        // Caclulate the manhattan distance between the current and lowest
        // adjacent cells.
        let currentManhattanDistance = Cell.manhattanDistance(
          currentCell,
          currentLowestValuedCell
        );

        // If adjacent cell is orthogonally adjacent, assign current cell a
        // floor field value of that cell + 1.
        // If adjacent cell is diagonal, assign current cell a floor
        // field value of that cell + LAMBDA
        this._floorField[currentCell.x][currentCell.y] =
          this._floorField[currentLowestValuedCell.x][
            currentLowestValuedCell.y
          ] + (currentManhattanDistance == 1 ? 1 : Configuration.LAMBDA);
      });

      // Move current cells to previous cells.
      previousCells = _.cloneDeep(currentCells);

      // Find adjacent cells of previously assigned cells yet to be assigned
      // a floor field value.
      currentCells = Cell.getAdjacentCellsOfCellsWithValue(
        this._floorField,
        previousCells,
        0
      );
    }
  }

  /**
   * Finds the cell adjacent to a given cell that has the lowest floor field
   * value.
   * @param c Cell for which to find an adjacent cell with the lowest floor
   * field value.
   * @returns Cell adjacent to c with the lowest floor field value.
   */
  private getLowestFloorFieldAdjacentCell(c: Cell): Cell {
    // Find all cells adjacent to c.
    let adjacentCells: Cell[] = Cell.getAdjacentCellsOfCell(
      this._configuration.width,
      this._configuration.height,
      c
    );

    // Get floor field values of adjacent cells and manhattan distances.
    let adjacentCellValues = adjacentCells.map((cc) => {
      return {
        cell: cc,
        value: this._floorField[cc.x][cc.y],
        manhattanDistance: Cell.manhattanDistance(c, cc),
      };
    });

    // Filter for cells yet to have a floor field value assigned.
    adjacentCellValues = adjacentCellValues.filter((cv) => cv.value > 0);

    // Sort cells by value, if tied, prefer orthoganally adjacent cells over
    // diagonal, (lowest manhattan distance).
    adjacentCellValues = adjacentCellValues.sort((a, b) => {
      if (a.value != b.value) {
        return a.value - b.value;
      } else {
        return a.manhattanDistance - b.manhattanDistance;
      }
    });

    // Return cell with lowest floor field value.
    return new Cell(adjacentCellValues[0].cell.x, adjacentCellValues[0].cell.y);
  }
}
