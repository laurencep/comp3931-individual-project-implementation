import Configuration from "./Configuration";
import Event from "./Event";
import Cell from "./Cell";

describe("import", () => {
  test("returns a configuration given a valid snapshot", () => {
    expect(
      typeof Configuration.import({
        width: 5,
        height: 5,
        doors: [new Cell(0, 2)],
        obstacles: [new Cell(1, 2), new Cell(1, 3)],
        pedestrians: [new Cell(2, 2), new Cell(2, 3)],
        panic: 0.05,
      })
    ).toBe("object");
  });

  test("throws an error if the dimensions are too small", () => {
    expect(() => {
      Configuration.import({
        width: 2,
        height: 2,
        doors: [],
        obstacles: [],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if there are duplicate doors", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [new Cell(0, 2), new Cell(0, 2)],
        obstacles: [],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if a door is not on perimeter", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [new Cell(1, 2)],
        obstacles: [],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if a door is outside the room", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [new Cell(-1, 2)],
        obstacles: [],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if there are duplicate obstacles", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [new Cell(1, 2), new Cell(1, 2)],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if an obstacle is on perimeter", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [new Cell(0, 3)],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if an obstacle is outside the room", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [new Cell(-1, 3)],
        pedestrians: [],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if there are duplicate pedestrians", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [],
        pedestrians: [new Cell(1, 2), new Cell(1, 2)],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if a pedestrian is on perimeter", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [],
        pedestrians: [new Cell(0, 3)],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if a pedestrian is outside the room", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [],
        pedestrians: [new Cell(-1, 3)],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if an obstacle and pedestrian overlap", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [new Cell(1, 3)],
        pedestrians: [new Cell(1, 3)],
        panic: 0.05,
      });
    }).toThrow();
  });

  test("throws an error if the panic value is too small", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [],
        pedestrians: [],
        panic: -0.1,
      });
    }).toThrow();
  });

  test("throws an error if the panic value is too large", () => {
    expect(() => {
      Configuration.import({
        width: 5,
        height: 5,
        doors: [],
        obstacles: [],
        pedestrians: [],
        panic: 1.1,
      });
    }).toThrow();
  });
});

describe("width", () => {
  test("returns width of room", () => {
    expect(typeof new Configuration().width).toBe("number");
  });
});

describe("height", () => {
  test("returns height of room", () => {
    expect(typeof new Configuration().height).toBe("number");
  });
});

describe("doors", () => {
  test("returns list of doors", () => {
    expect(
      new Configuration().doors.every((door) => door instanceof Cell)
    ).toBe(true);
  });
});

describe("obstacles", () => {
  test("returns list of obstacles", () => {
    expect(
      new Configuration().obstacles.every(
        (obstacle) => obstacle instanceof Cell
      )
    ).toBe(true);
  });
});

describe("pedestrians", () => {
  test("returns list of pedestrians", () => {
    expect(
      new Configuration().pedestrians.every(
        (pedestrian) => pedestrian instanceof Cell
      )
    ).toBe(true);
  });
});

describe("panic", () => {
  test("returns panic value", () => {
    expect(typeof new Configuration().panic).toBe("number");
  });

  test("allows itself to be set to a valid value", () => {
    let c = new Configuration();
    c.panic = 30;
    expect(c.panic).toBe(0.3);
  });

  test("throws error if set outside valid range", () => {
    expect(() => (new Configuration().panic = 120)).toThrow();
  });

  test("triggers modified event when changed", () => {
    let c = new Configuration();
    let handler = jest.fn();
    c.modified.subscribe(handler);
    c.panic = 50;
    expect(handler).toBeCalled();
  });
});

describe("modfied", () => {
  test("returns the modified event", () => {
    expect(new Configuration().modified instanceof Event).toBe(true);
  });
});

describe("toggleDoor", () => {
  test("places a door in a cell if on perimeter and not already door", () => {
    let c = new Configuration();
    c.toggleDoor(new Cell(0, 2));
    expect(c.doors.some((door) => door.x == 0 && door.y == 2)).toBe(true);
  });

  test("removes a door from a cell if already a door", () => {
    let c = new Configuration();
    c.toggleDoor(new Cell(0, 2));
    c.toggleDoor(new Cell(0, 2));
    expect(c.doors.some((door) => door.x == 0 && door.y == 2)).toBe(false);
  });

  test("will not place a door if in the room but not on the perimeter", () => {
    let c = new Configuration();
    c.toggleDoor(new Cell(1, 2));
    expect(c.doors.some((door) => door.x == 1 && door.y == 2)).toBe(false);
  });

  test("will not place a door if outside of room", () => {
    let c = new Configuration();
    c.toggleDoor(new Cell(-1, 2));
    expect(c.doors.some((door) => door.x == -1 && door.y == 2)).toBe(false);
  });

  test("triggers modified event", () => {
    let c = new Configuration();
    let handler = jest.fn();
    c.modified.subscribe(handler);
    c.toggleDoor(new Cell(0, 2));
    expect(handler).toBeCalled();
  });
});

describe("toggleObstacle", () => {
  test("places an obstacle in cell if in room and not already there", () => {
    let c = new Configuration();
    c.toggleObstacle(new Cell(1, 2));
    expect(
      c.obstacles.some((obstacle) => obstacle.x == 1 && obstacle.y == 2)
    ).toBe(true);
  });

  test("removes an obstacle from a cell if already an obstacle", () => {
    let c = new Configuration();
    c.toggleObstacle(new Cell(1, 2));
    c.toggleObstacle(new Cell(1, 2));
    expect(
      c.obstacles.some((obstacle) => obstacle.x == 1 && obstacle.y == 2)
    ).toBe(false);
  });

  test("will not place an obstacle if on the perimeter", () => {
    let c = new Configuration();
    c.toggleObstacle(new Cell(0, 2));
    expect(
      c.obstacles.some((obstacle) => obstacle.x == 0 && obstacle.y == 2)
    ).toBe(false);
  });

  test("will not place an obstacle if outside the room", () => {
    let c = new Configuration();
    c.toggleObstacle(new Cell(-1, 2));
    expect(
      c.obstacles.some((obstacle) => obstacle.x == -1 && obstacle.y == 2)
    ).toBe(false);
  });

  test("will not place an obstacle on a cell containing a pedestrian", () => {
    let c = new Configuration();
    c.togglePedestrian(new Cell(1, 2));
    c.toggleObstacle(new Cell(1, 2));
    expect(
      c.obstacles.some((obstacle) => obstacle.x == 1 && obstacle.y == 2)
    ).toBe(false);
  });

  test("triggers modified event", () => {
    let c = new Configuration();
    let handler = jest.fn();
    c.modified.subscribe(handler);
    c.toggleObstacle(new Cell(1, 2));
    expect(handler).toBeCalled();
  });
});

describe("togglePedestrian", () => {
  test("places a pedestrian in cell if in room and not already there", () => {
    let c = new Configuration();
    c.togglePedestrian(new Cell(1, 2));
    expect(
      c.pedestrians.some((pedestrian) => pedestrian.x == 1 && pedestrian.y == 2)
    ).toBe(true);
  });

  test("removes a pedestrian from a cell if already a pedestrian", () => {
    let c = new Configuration();
    c.togglePedestrian(new Cell(1, 2));
    c.togglePedestrian(new Cell(1, 2));
    expect(
      c.pedestrians.some((pedestrian) => pedestrian.x == 1 && pedestrian.y == 2)
    ).toBe(false);
  });

  test("will not place a pedestrian if on the perimeter", () => {
    let c = new Configuration();
    c.togglePedestrian(new Cell(0, 2));
    expect(
      c.pedestrians.some((pedestrian) => pedestrian.x == 0 && pedestrian.y == 2)
    ).toBe(false);
  });

  test("will not place a pedestrian if outside the room", () => {
    let c = new Configuration();
    c.togglePedestrian(new Cell(-1, 2));
    expect(
      c.pedestrians.some(
        (pedestrian) => pedestrian.x == -1 && pedestrian.y == 2
      )
    ).toBe(false);
  });

  test("will not place a pedestrian on a cell containing an obstacle", () => {
    let c = new Configuration();
    c.toggleObstacle(new Cell(1, 2));
    c.togglePedestrian(new Cell(1, 2));
    expect(
      c.pedestrians.some((pedestrian) => pedestrian.x == 1 && pedestrian.y == 2)
    ).toBe(false);
  });

  test("triggers modified event", () => {
    let c = new Configuration();

    let handler = jest.fn();

    c.modified.subscribe(handler);

    c.togglePedestrian(new Cell(1, 2));

    expect(handler).toBeCalled();
  });
});

describe("clearDoors", () => {
  test("removes all placed doors", () => {
    let c = new Configuration();
    c.clearDoors();
    expect(c.doors.length).toBe(0);
  });
});

describe("clearObstacles", () => {
  test("removes all placed obstacles", () => {
    let c = new Configuration();
    c.clearObstacles();
    expect(c.obstacles.length).toBe(0);
  });
});

describe("clearPedestrians", () => {
  test("removes all placed pedestrians", () => {
    let c = new Configuration();
    c.clearPedestrians();
    expect(c.pedestrians.length).toBe(0);
  });
});

describe("placeRandomPedestrians", () => {
  test("places correct amount of pedestrians in room", () => {
    let c = new Configuration();
    c.clearPedestrians();
    c.placeRandomPedestrians(5);
    expect(c.pedestrians.length).toBe(5);
  });

  test("places no pedestrians if 0 specified", () => {
    let c = new Configuration();
    c.clearPedestrians();
    c.placeRandomPedestrians(0);
    expect(c.pedestrians.length).toBe(0);
  });

  test("places as many as possible if number more than space in room", () => {
    let c = new Configuration();
    c.clearDoors();
    c.clearObstacles();
    c.clearPedestrians();
    c.changeDimensions(5, 5);
    c.toggleDoor(new Cell(0, 2));
    c.placeRandomPedestrians(2000);
    expect(c.pedestrians.length).toBe(9);
  });
});

describe("changeDimensions", () => {
  test("changes dimensions if given valid dimensions", () => {
    let c = new Configuration();
    c.changeDimensions(5, 6);
    expect(c.width).toBe(5);
    expect(c.height).toBe(6);
  });

  test("throws error if width too small", () => {
    let c = new Configuration();
    expect(() => c.changeDimensions(2, 3)).toThrow();
  });

  test("throws error if height too small", () => {
    let c = new Configuration();
    expect(() => c.changeDimensions(3, 2)).toThrow();
  });

  test("removes doors that are no longer on the perimeter", () => {
    let c = new Configuration();
    c.changeDimensions(5, 5);
    c.toggleDoor(new Cell(2, 4));
    c.changeDimensions(5, 4);
    expect(c.doors.some((door) => door.x == 2 && door.y == 4)).toBe(false);
  });

  test("removes obstacles not within the area and not on perimeter", () => {
    let c = new Configuration();
    c.changeDimensions(5, 5);
    c.toggleObstacle(new Cell(2, 3));
    c.changeDimensions(5, 4);
    expect(
      c.obstacles.some((obstacle) => obstacle.x == 2 && obstacle.y == 3)
    ).toBe(false);
  });

  test("removes pedestrians not within the area and not on perimeter", () => {
    let c = new Configuration();
    c.changeDimensions(5, 5);
    c.togglePedestrian(new Cell(2, 3));
    c.changeDimensions(5, 4);
    expect(
      c.pedestrians.some((pedestrian) => pedestrian.x == 2 && pedestrian.y == 3)
    ).toBe(false);
  });

  test("triggers modified event", () => {
    let c = new Configuration();
    let handler = jest.fn();
    c.modified.subscribe(handler);
    c.changeDimensions(5, 5);
    expect(handler).toBeCalled();
  });
});

describe("snapshot", () => {
  test("returns valid snapshot", () => {
    let c = new Configuration();
    c.clearDoors();
    c.clearObstacles();
    c.clearPedestrians();
    c.changeDimensions(5, 5);
    c.toggleDoor(new Cell(0, 2));
    c.toggleObstacle(new Cell(2, 3));
    c.togglePedestrian(new Cell(2, 2));
    c.panic = 50;
    expect(c.snapshot()).toEqual({
      width: 5,
      height: 5,
      doors: [new Cell(0, 2)],
      obstacles: [new Cell(2, 3)],
      pedestrians: [new Cell(2, 2)],
      panic: 0.5,
    });
  });
});
