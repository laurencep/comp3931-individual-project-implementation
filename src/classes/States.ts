/**
 * Represents the different possible states of simulations, the application and
 * the client.
 */
export enum State {
  Doors, // Placing doors on the configuration
  Obstacles, // Placing obstacles on the configuration
  Pedestrians, // Placing pedestrians on the configuration
  Simulating, // Simulation has started
  Initialised, // Simulation has been created but not started.
  Trapped, // Configuration has pedestrians that cannot evacuate.
  Running, // Simulation is running at defined interval.
  Paused, // Simulation is paused.
  Completing, // Simulation is completing as fast as possible.
  Completed, // Simulation has finished completing as fast as possible.
  Finished, // Simulation has finished.
}

/**
 * Represents the state of a simulation.
 */
export type SimulationState =
  | State.Initialised
  | State.Running
  | State.Paused
  | State.Finished;

/**
 * Represents the state of the application.
 */
export type ApplicationState =
  | State.Doors
  | State.Obstacles
  | State.Pedestrians
  | State.Completing
  | State.Completed
  | State.Simulating
  | State.Trapped;

/**
 * Represents the state of the client.
 */
export type ClientState =
  | State.Doors
  | State.Obstacles
  | State.Pedestrians
  | State.Running
  | State.Paused
  | State.Completing
  | State.Finished
  | State.Trapped;
