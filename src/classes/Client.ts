import _ from "lodash";
import { Socket } from "socket.io";
import Cell from "./Cell";
import Configuration from "./Configuration";
import Simulation, { TrappedPedestrianError } from "./Simulation";
import SimulationSeries from "./SimulationSeries";
import { ClientSnapshot } from "./Snapshots";
import { ApplicationState, ClientState, State } from "./States";

/**
 * Represents a client connected to the web application.
 */
export default class Client {
  // Socket.io socket for client.
  private _socket: Socket;

  // Application state for client.
  private _state: ApplicationState = State.Doors;

  // Client's current configuration.
  private _configuration: Configuration = new Configuration();

  // Client's current simulation.
  private _simulation: Simulation | null = null;

  // Client's current simulations series.
  private _simulationSeries: SimulationSeries | null = null;

  // Client's current interval.
  private _interval: number = 1000;

  /**
   * Create a new client from a given Socket.io socket.
   * @param socket Socket.io socket of client.
   */
  constructor(socket: Socket) {
    // Set the socket.
    this._socket = socket;

    // Send an update to client whenever configuration changes.
    this._configuration.modified.subscribe(this.updateClient.bind(this));

    // Setup event listeners for socket messages.
    this.setupSocket();

    // Send initial update to client.
    this.updateClient();
  }

  /**
   * Current configuration of client.
   */
  get configuration() {
    return this._configuration;
  }

  /**
   * Socket ID associated with client.
   */
  get socketID() {
    return this._socket.id;
  }

  /**
   * Can be set to any configuration, will clear any simulation or simulation
   * series.
   */
  set configuration(configuration: Configuration) {
    this._state = State.Doors;
    this._simulation = null;
    this._simulationSeries = null;
    this._configuration = configuration;
    this._configuration.modified.subscribe(this.updateClient.bind(this));
    this.updateClient();
  }

  /**
   * Sets up listeners for incoming messages from the client over Socket.io.
   */
  private setupSocket(): void {
    /**
     * Handles click messages from the client, performs correct action based on
     * state.
     */
    this._socket.on("click", (cell: Cell) => {
      // Toggle correct object based on current state.
      switch (this._state) {
        case State.Doors: {
          this._configuration.toggleDoor(cell);
          break;
        }
        case State.Obstacles: {
          this._configuration.toggleObstacle(cell);
          break;
        }
        case State.Pedestrians: {
          this._configuration.togglePedestrian(cell);
          break;
        }
      }
    });

    /**
     * Handles play messages from the client, starts the simulation.
     */
    this._socket.on("play", () => {
      // If first time play button has been pressed, create simulation.
      if (this._state != State.Simulating) {
        // Update state.
        this._state = State.Simulating;

        // Create new simulation from configuration.
        try {
          this._simulation = new Simulation(this._configuration);

          // Pass interval to simulation.
          this._simulation.interval = this._interval;

          // Update client when simulation updates.
          this._simulation.updated.subscribe(this.updateClient.bind(this));

          // Start simulation.
          this._simulation.play();

          // Catch cases where there are trapped pedestrians.
        } catch (e) {
          if (e instanceof TrappedPedestrianError) {
            this._state = State.Trapped;
            this.updateClient();
          } else {
            throw e;
          }
        }

        // Otherwise, simulation was paused, so just start it again.
      } else {
        this._simulation!.play();
      }
    });

    /**
     * Handles pause messages from the client, pauses the simulation if it is
     * running.
     */
    this._socket.on("pause", () => {
      if (this._state != State.Simulating) {
        throw new Error("No simulation running.");
      } else {
        this._simulation!.pause();
      }
    });

    /**
     * Handles step messages from the client, progresses the simulation by one
     * time step.
     */
    this._socket.on("step", () => {
      // If simulation wasn't running, create new simulation and step.
      if (this._state != State.Simulating) {
        // Update state.
        this._state = State.Simulating;

        // Create new simulation from configuration.
        try {
          this._simulation = new Simulation(this._configuration);

          // Pass interval to simulation.
          this._simulation.interval = this._interval;

          // Update client when simulation updates.
          this._simulation.updated.subscribe(this.updateClient.bind(this));

          // Perform single step.
          this._simulation.step();

          // Catch cases where there are trapped pedestrians.
        } catch (e) {
          if (e instanceof TrappedPedestrianError) {
            this._state = State.Trapped;
            this.updateClient();
          } else {
            throw e;
          }
        }

        // Otherwise, simulation was paused, perform single step.
      } else {
        this._simulation!.step();
      }
    });

    /**
     * Handles reset messages from the client, clears simulation and updates
     * state.
     */
    this._socket.on("reset", () => {
      this._state = State.Doors;
      this._simulation = null;
      this.updateClient();
    });

    /**
     * Handles complete messages from the client, completes the requested number
     * of simulations.
     */
    this._socket.on("complete", (m) => {
      // Update state.
      this._state = State.Completing;

      // Send updated state to client.
      this.updateClient();

      // Create new simulation series and complete.
      try {
        this._simulationSeries = new SimulationSeries(this._configuration, m);
        this._simulationSeries.run();

        // Update state.
        this._state = State.Completed;

        // Send updated state and results to client.
        this.updateClient();
        // Catch cases where there are trapped pedestrians.
      } catch (e) {
        if (e instanceof TrappedPedestrianError) {
          this._state = State.Trapped;
          this.updateClient();
        } else {
          throw e;
        }
      }
    });

    /**
     * Handles interval change messages from the client, updates current interval
     * and simulation if needed.
     */
    this._socket.on("changeInterval", (i) => {
      this._interval = i;
      if (this._simulation != null) {
        this._simulation.interval = this._interval;
      }
    });

    /**
     * Handles dimension change messages from the client, passes changes to
     * configuration.
     */
    this._socket.on("changeDimensions", (width, height) => {
      this._configuration.changeDimensions(width, height);
    });

    /**
     * Handles door placement messages from the client, updates state and updates
     * client.
     */
    this._socket.on("placeDoors", () => {
      this._state = State.Doors;
      this.updateClient();
    });

    /**
     * Handles clear door messages from the client, clears doors and updates
     * client.
     */
    this._socket.on("clearDoors", () => {
      this._configuration.clearDoors();
      this.updateClient();
    });

    /**
     * Handles obstacle placement messages from the client, updates state and
     * updates client.
     */
    this._socket.on("placeObstacles", () => {
      this._state = State.Obstacles;
      this.updateClient();
    });

    /**
     * Handles clear obstacles messages from the client, clears obstacles and
     * updates client.
     */
    this._socket.on("clearObstacles", () => {
      this._configuration.clearObstacles();
      this.updateClient();
    });

    /**
     * Handles pedestrian placement messages from the client, updates state and
     * updates client.
     */
    this._socket.on("placePedestrians", () => {
      this._state = State.Pedestrians;
      this.updateClient();
    });

    /**
     * Handles clear pedestrian messages from the client, clears pedestrians and
     * updates client.
     */
    this._socket.on("clearPedestrians", () => {
      this._configuration.clearPedestrians();
      this.updateClient();
    });

    /**
     * Handles random pedestrian placement messages from the client, passes the
     * request onto the configuration.
     */
    this._socket.on("placeRandomPedestrians", (n) => {
      this._configuration.placeRandomPedestrians(n);
    });

    /**
     * Handles panic change request messages from the client, passes the request
     * on to the configuration.
     */
    this._socket.on("changePanic", (panic) => {
      this._configuration.panic = panic;
    });
  }

  /**
   * Generates and sends an update to the client.
   */
  private updateClient(): void {
    let clientSnapshot: ClientSnapshot;

    // Generate a client snapshot depending on state.
    switch (this._state) {
      case State.Doors:
      case State.Obstacles:
      case State.Pedestrians:
      case State.Completing:
      case State.Trapped: {
        clientSnapshot = {
          state: _.cloneDeep(this._state) as ClientState,
          width: this._configuration.width,
          height: this._configuration.height,
          doors: _.cloneDeep(this._configuration.doors),
          obstacles: _.cloneDeep(this._configuration.obstacles),
          pedestrians: _.cloneDeep(this._configuration.pedestrians),
          panic: this._configuration.panic,
          time: 0,
          pedestrianCountsAtTime: [],
          averageTime: null,
          interval: this._interval,
        };
        break;
      }
      case State.Simulating: {
        clientSnapshot = {
          state: _.cloneDeep(this._simulation!.state) as ClientState,
          width: this._simulation!.configuration.width,
          height: this._simulation!.configuration.height,
          doors: _.cloneDeep(this._simulation!.configuration.doors),
          obstacles: _.cloneDeep(this._simulation!.configuration.obstacles),
          pedestrians: _.cloneDeep(this._simulation!.pedestrians),
          panic: this._simulation!.configuration.panic,
          time: this._simulation!.time,
          pedestrianCountsAtTime: [
            _.cloneDeep(this._simulation!.pedestrianCountAtTime),
          ],
          averageTime: null,
          interval: this._simulation!.interval,
        };
        break;
      }
      case State.Completed: {
        clientSnapshot = {
          state: State.Finished,
          width: this._configuration.width,
          height: this._configuration.height,
          doors: _.cloneDeep(this._configuration.doors),
          obstacles: _.cloneDeep(this._configuration.obstacles),
          pedestrians: _.cloneDeep(this._configuration.pedestrians),
          panic: this._configuration.panic,
          time: 0,
          pedestrianCountsAtTime: _.cloneDeep(
            this._simulationSeries!.pedestrianCountsAtTime
          ),
          averageTime: this._simulationSeries!.averageTime,
          interval: this._interval,
        };
        break;
      }
    }

    // Emit update to client.
    this._socket.emit("update", clientSnapshot);
  }
}
