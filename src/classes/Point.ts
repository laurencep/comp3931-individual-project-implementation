/**
 * Represents a point in a 2D area defined by x and y.
 */
export default class Point {
  readonly x: number; // x coordinate.
  readonly y: number; // y coordinate.

  /**
   * Creates a new point from a set of x and y coordinates.
   * @param x x coordinate.
   * @param y y coordinate.
   */
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}
