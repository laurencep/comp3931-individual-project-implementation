import Cell from "./Cell";
import Move from "./Move";

describe("current", () => {
  test("returns current move", () => {
    expect(new Move(new Cell(5, 5), new Cell(5, 6)).current).toEqual(
      new Cell(5, 5)
    );
  });
});

describe("next", () => {
  test("returns next move", () => {
    expect(new Move(new Cell(5, 5), new Cell(5, 6)).next).toEqual(
      new Cell(5, 6)
    );
  });
});
