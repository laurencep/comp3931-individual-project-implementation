import { io } from "socket.io-client";
import Cell from "../classes/Cell";
import { ClientSnapshot } from "../classes/Snapshots";
import { State } from "../classes/States";
import Point from "../classes/Point";
import _ from "lodash";
import Chart, { ScatterDataPoint } from "chart.js/auto";

// Initialise socket.io.
const socket = io();

// Create reference to main canvas and get context.
const canvas = document.getElementById("canvas") as HTMLCanvasElement;
const ctx = canvas.getContext("2d")!;
const PPC = 20; // Pixels per cell.

// Create references to help overlay and help buttons.
const helpOverlay = document.getElementById("help-overlay") as HTMLDivElement;
const showHelpButton = document.getElementById(
  "show-help"
) as HTMLButtonElement;
const closeHelpButton = document.getElementById(
  "close-help"
) as HTMLButtonElement;

// Show or hide help overlay when buttons pressed.
showHelpButton.addEventListener("click", () => {
  helpOverlay.style.display = "block";
});

closeHelpButton.addEventListener("click", () => {
  helpOverlay.style.display = "none";
});

// Create references to simulation controls.
const playButton = document.getElementById("play") as HTMLButtonElement;
const pauseButton = document.getElementById("pause") as HTMLButtonElement;
const stepButton = document.getElementById("step") as HTMLButtonElement;
const resetButton = document.getElementById("reset") as HTMLButtonElement;
const completeButton = document.getElementById("complete") as HTMLButtonElement;
const mInput = document.getElementById("m") as HTMLInputElement;
const intervalSlider = document.getElementById("interval") as HTMLInputElement;
const intervalValue = document.getElementById(
  "interval-value"
) as HTMLParagraphElement;

// Create references to configuration controls.
const nameInput = document.getElementById("name") as HTMLInputElement;
const widthInput = document.getElementById("width") as HTMLInputElement;
const heightInput = document.getElementById("height") as HTMLInputElement;
const placeDoorsButton = document.getElementById(
  "place-doors"
) as HTMLButtonElement;
const clearDoorsButton = document.getElementById(
  "clear-doors"
) as HTMLButtonElement;
const placeObstaclesButton = document.getElementById(
  "place-obstacles"
) as HTMLButtonElement;
const clearObstaclesButton = document.getElementById(
  "clear-obstacles"
) as HTMLButtonElement;
const placePedestriansButton = document.getElementById(
  "place-pedestrians"
) as HTMLButtonElement;
const clearPedestriansButton = document.getElementById(
  "clear-pedestrians"
) as HTMLButtonElement;
const placeRandomPedestriansButton = document.getElementById(
  "place-random-pedestrians"
) as HTMLButtonElement;
const nInput = document.getElementById("n") as HTMLInputElement;
const panicInput = document.getElementById("panic") as HTMLInputElement;
const exportButton = document.getElementById("export") as HTMLButtonElement;
const importButton = document.getElementById(
  "import-input"
) as HTMLInputElement;
const importInvalidLabel = document.getElementById(
  "import-invalid"
) as HTMLParagraphElement;

// Create references to timer elements.
const timeLabel = document.getElementById("time") as HTMLParagraphElement;
const averageTimeLabel = document.getElementById(
  "averageTime-value"
) as HTMLSpanElement;
const statusLabel = document.getElementById(
  "statusMessage"
) as HTMLParagraphElement;

// Colors to be used for graph plots
const COLORS = [
  "#4E79A7",
  "#F28E2B",
  "#E15759",
  "#76B7B2",
  "#59A14F",
  "#EDC948",
  "#B07AA1",
  "#FF9DA7",
  "#9C755F",
  "#BAB0AC",
];

// Create references to chart and chart controls.
const chartCanvas = document.getElementById("chart") as HTMLCanvasElement;
const clearChartButton = document.getElementById(
  "clear-chart"
) as HTMLButtonElement;
const exportResultsButton = document.getElementById(
  "export-results"
) as HTMLButtonElement;

// Create chart.
const chart = new Chart(chartCanvas.getContext("2d")!, {
  type: "line",
  data: {
    datasets: [],
  },
  options: {
    scales: {
      x: {
        type: "linear",
        beginAtZero: true,
        title: {
          display: true,
          text: "Time",
        },
        ticks: {
          stepSize: 1,
        },
      },
      y: {
        type: "linear",
        beginAtZero: true,
        title: {
          display: true,
          text: "Pedestrians in Room",
        },
        ticks: {
          stepSize: 1,
        },
      },
    },
    plugins: {
      legend: {
        position: "right",
      },
    },
  },
});

// Holds the latest snapshot received from the server.
let snapshot: ClientSnapshot;

/**
 * Handles receiving an update from the server.
 */
socket.on("update", (s: ClientSnapshot) => {
  // Update current snapshot.
  snapshot = s;

  // Setup UI, update control states, draw room and update graph.
  setup();
  updateState();
  draw();
  updateGraph();
});

// Holds the last cell that the user clicked or dragged on.
let currentCell: Cell;

/**
 * Handles the user pressing the mouse button on the main canvas.
 */
canvas.addEventListener("mousedown", (e) => {
  // Determine the cell that the mouse is currently on.
  currentCell = Cell.fromPoint(
    new Point(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop),
    PPC
  );

  // Send a click event to the server.
  socket.emit("click", currentCell);

  // Listen for any mouse movements indicating a drag.
  canvas.addEventListener("mousemove", drag);
});

/**
 * Handles the user releasing the mouse button on the main canvas.
 */
canvas.addEventListener("mouseup", (e) => {
  // Stop listening for mouse movements, indicating the end of a drag.
  canvas.removeEventListener("mousemove", drag);
});

/**
 * Called when the mouse is moved while holding the mouse button on the canvas.
 */
function drag(e: MouseEvent) {
  // Find the cell the mouse is now in.
  let newCell = Cell.fromPoint(
    new Point(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop),
    PPC
  );
  // If it is different, send it to the server.
  if (!_.isEqual(currentCell, newCell)) {
    currentCell = newCell;
    socket.emit("click", currentCell);
  }
}

/**
 * Handles play button being pressed, sends message to server.
 */
playButton.addEventListener("click", () => socket.emit("play"));

/**
 * Handles pause button being pressed, sends message to server.
 */
pauseButton.addEventListener("click", () => socket.emit("pause"));

/**
 * Handles step button being pressed, sends message to server.
 */
stepButton.addEventListener("click", () => socket.emit("step"));

/**
 * Handles reset button being pressed, sends message to server.
 */
resetButton.addEventListener("click", () => socket.emit("reset"));

/**
 * Handles complete button being pressed, sends message to server.
 */
completeButton.addEventListener("click", () =>
  socket.emit("complete", mInput.value)
);

// True if user currently sliding interval slider, false otherwise.
let changingInterval = false;

/**
 * Handles interval slider sliding, updates the UI label.
 */
intervalSlider.addEventListener(
  "input",
  () => (intervalValue.textContent = intervalSlider.value)
);

/**
 * Handles the interval slider stopping sliding, sends new value to server.
 */
intervalSlider.addEventListener("change", () =>
  socket.emit("changeInterval", intervalSlider.value)
);

/**
 * Handles interval slider input beginning, sets changing variable to true.
 */
intervalSlider.addEventListener("mousedown", () => {
  changingInterval = true;
});

/**
 * Handles interval slider input stoppoing, sets changing variable to false.
 */
intervalSlider.addEventListener("mouseup", () => {
  changingInterval = false;
});

/**
 * Handles width input changing, sends new dimensions to server.
 */
widthInput.addEventListener("change", () => {
  socket.emit("changeDimensions", widthInput.value, heightInput.value);
});

/**
 * Handles height input changing, sends new dimensions to server.
 */
heightInput.addEventListener("change", () => {
  socket.emit("changeDimensions", widthInput.value, heightInput.value);
});

/**
 * Handles place doors button being pressed, sends message to server.
 */
placeDoorsButton.addEventListener("click", () => {
  socket.emit("placeDoors");
});

/**
 * Handles clear doors button being pressed, sends message to server.
 */
clearDoorsButton.addEventListener("click", () => {
  socket.emit("clearDoors");
});

/**
 * Handles place obstacles button being pressed, sends message to server.
 */
placeObstaclesButton.addEventListener("click", () => {
  socket.emit("placeObstacles");
});

/**
 * Handles clear obstacles button being pressed, sends message to server.
 */
clearObstaclesButton.addEventListener("click", () => {
  socket.emit("clearObstacles");
});

/**
 * Handles place pedestrians button being pressed, sends message to server.
 */
placePedestriansButton.addEventListener("click", () => {
  socket.emit("placePedestrians");
});

/**
 * Handles clear pedestrians button being pressed, sends message to server.
 */
clearPedestriansButton.addEventListener("click", () => {
  socket.emit("clearPedestrians");
});

/**
 * Handles place random pedestrians button being pressed, sends message to
 * server.
 */
placeRandomPedestriansButton.addEventListener("click", () => {
  socket.emit("placeRandomPedestrians", nInput.value);
});

/**
 * Handles panic input changing, sends updated value to server.
 */
panicInput.addEventListener("change", () => {
  socket.emit("changePanic", panicInput.value);
});

/**
 * Handles export button being pressed, sends export request to server and
 * downloads result.
 */
exportButton.addEventListener("click", async () => {
  // Send request.
  let response = await fetch(`/export/${socket.id}`);

  // Parse result into file.
  let json = await response.json();
  let file = new Blob([JSON.stringify(json)], { type: "application/json" });

  // Download file.
  let a = document.createElement("a");
  a.href = URL.createObjectURL(file);
  a.download = "configuration.json";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
});

/**
 * Handles import button being pressed, sends import file to server.
 */
importButton.onchange = async () => {
  // Send request.
  let formData = new FormData();
  formData.append("configuration", importButton.files![0]);
  let response = await fetch(`/import/${socket.id}`, {
    method: "POST",
    body: formData,
  });

  // If import failed, display error message.
  if (response.status != 200) {
    importInvalidLabel.style.display = "block";
  } else {
    importInvalidLabel.style.display = "none";
  }
};

/**
 * Handles clear chart button being pressed, deletes all datasets and updates.
 */
clearChartButton.addEventListener("click", () => {
  chart.data.datasets = [];
  chart.update();
});

/**
 * Handles export results button being pressed, downloads results to file.
 */
exportResultsButton.addEventListener("click", () => {
  // Map chart datasets to JSON object.
  let exportData = chart.data.datasets.map((dataset) => ({
    label: dataset.label,
    pedestriansAtTime: dataset.data.map((dataPoint) => ({
      time: (dataPoint as ScatterDataPoint).x,
      pedestrians: (dataPoint as ScatterDataPoint).y,
    })),
    totalEvacuationTime: dataset.data.length - 1,
    averageEvacuationTime:
      (dataset.pointRadius as number[]).indexOf(6) != -1
        ? (dataset.pointRadius as number[]).indexOf(6)
        : "N/A",
  }));

  // Create download link and attatch JSON string.
  let downloadLink = document.createElement("a");
  downloadLink.href = `data:text/json;charset=utf-8,${encodeURIComponent(
    JSON.stringify(exportData)
  )}`;
  downloadLink.download = "results.json";
  downloadLink.style.display = "none";

  // Append download link, click it and then remove it.
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
});

/**
 * Sets up the user interface, assigning values to the labels.
 */
function setup(): void {
  // Set canvas height.
  canvas.width = snapshot.width * PPC;
  canvas.height = snapshot.height * PPC;

  // Set configuration input values.
  widthInput.value = `${snapshot.width}`;
  heightInput.value = `${snapshot.height}`;
  panicInput.value = `${snapshot.panic * 100}`;

  // Is not changing interval slider, set value and label.
  if (!changingInterval) {
    intervalSlider.value = `${snapshot.interval}`;
    intervalValue.textContent = `${snapshot.interval}`;
  }

  // Set time labels.
  timeLabel.textContent = `${snapshot.time}`;
  averageTimeLabel.textContent = `${
    snapshot.averageTime != null ? _.round(snapshot.averageTime, 2) : "N/A"
  }`;

  // Set status message.
  statusLabel.textContent = getStatusMessage();
}

/**
 * Enables or disables controls based on current state.
 */
function updateState(): void {
  switch (snapshot.state) {
    case State.Doors: {
      playButton.disabled = false;
      pauseButton.disabled = true;
      stepButton.disabled = false;
      resetButton.disabled = true;
      completeButton.disabled = false;
      mInput.disabled = false;
      intervalSlider.disabled = false;
      nameInput.disabled = false;
      widthInput.disabled = false;
      heightInput.disabled = false;
      placeDoorsButton.disabled = true;
      placeObstaclesButton.disabled = false;
      placePedestriansButton.disabled = false;
      clearDoorsButton.disabled = false;
      clearObstaclesButton.disabled = false;
      clearPedestriansButton.disabled = false;
      placeRandomPedestriansButton.disabled = false;
      nInput.disabled = false;
      panicInput.disabled = false;
      exportButton.disabled = false;
      clearChartButton.disabled = false;
      exportResultsButton.disabled = false;
      break;
    }
    case State.Obstacles: {
      playButton.disabled = false;
      pauseButton.disabled = true;
      stepButton.disabled = false;
      resetButton.disabled = true;
      completeButton.disabled = false;
      mInput.disabled = false;
      intervalSlider.disabled = false;
      nameInput.disabled = false;
      widthInput.disabled = false;
      heightInput.disabled = false;
      placeDoorsButton.disabled = false;
      placeObstaclesButton.disabled = true;
      placePedestriansButton.disabled = false;
      clearDoorsButton.disabled = false;
      clearObstaclesButton.disabled = false;
      clearPedestriansButton.disabled = false;
      placeRandomPedestriansButton.disabled = false;
      nInput.disabled = false;
      panicInput.disabled = false;
      exportButton.disabled = false;
      clearChartButton.disabled = false;
      exportResultsButton.disabled = false;
      break;
    }
    case State.Pedestrians: {
      playButton.disabled = false;
      pauseButton.disabled = true;
      stepButton.disabled = false;
      resetButton.disabled = true;
      completeButton.disabled = false;
      mInput.disabled = false;
      intervalSlider.disabled = false;
      nameInput.disabled = false;
      widthInput.disabled = false;
      heightInput.disabled = false;
      placeDoorsButton.disabled = false;
      placeObstaclesButton.disabled = false;
      placePedestriansButton.disabled = true;
      clearDoorsButton.disabled = false;
      clearObstaclesButton.disabled = false;
      clearPedestriansButton.disabled = false;
      placeRandomPedestriansButton.disabled = false;
      nInput.disabled = false;
      panicInput.disabled = false;
      exportButton.disabled = false;
      clearChartButton.disabled = false;
      exportResultsButton.disabled = false;
      break;
    }
    case State.Running: {
      playButton.disabled = true;
      pauseButton.disabled = false;
      stepButton.disabled = true;
      resetButton.disabled = false;
      completeButton.disabled = true;
      mInput.disabled = true;
      intervalSlider.disabled = false;
      nameInput.disabled = true;
      widthInput.disabled = true;
      heightInput.disabled = true;
      placeDoorsButton.disabled = true;
      placeObstaclesButton.disabled = true;
      placePedestriansButton.disabled = true;
      clearDoorsButton.disabled = true;
      clearObstaclesButton.disabled = true;
      clearPedestriansButton.disabled = true;
      placeRandomPedestriansButton.disabled = true;
      nInput.disabled = true;
      panicInput.disabled = true;
      exportButton.disabled = true;
      clearChartButton.disabled = true;
      exportResultsButton.disabled = true;
      break;
    }
    case State.Paused: {
      playButton.disabled = false;
      pauseButton.disabled = true;
      stepButton.disabled = false;
      resetButton.disabled = false;
      completeButton.disabled = true;
      mInput.disabled = true;
      intervalSlider.disabled = false;
      nameInput.disabled = true;
      widthInput.disabled = true;
      heightInput.disabled = true;
      placeDoorsButton.disabled = true;
      placeObstaclesButton.disabled = true;
      placePedestriansButton.disabled = true;
      clearDoorsButton.disabled = true;
      clearObstaclesButton.disabled = true;
      clearPedestriansButton.disabled = true;
      placeRandomPedestriansButton.disabled = true;
      nInput.disabled = true;
      panicInput.disabled = true;
      exportButton.disabled = true;
      clearChartButton.disabled = true;
      exportResultsButton.disabled = true;
      break;
    }
    case State.Completing: {
      playButton.disabled = true;
      pauseButton.disabled = true;
      stepButton.disabled = true;
      resetButton.disabled = true;
      completeButton.disabled = true;
      mInput.disabled = true;
      intervalSlider.disabled = true;
      nameInput.disabled = true;
      widthInput.disabled = true;
      heightInput.disabled = true;
      placeDoorsButton.disabled = true;
      placeObstaclesButton.disabled = true;
      placePedestriansButton.disabled = true;
      clearDoorsButton.disabled = true;
      clearObstaclesButton.disabled = true;
      clearPedestriansButton.disabled = true;
      placeRandomPedestriansButton.disabled = true;
      nInput.disabled = true;
      panicInput.disabled = true;
      exportButton.disabled = true;
      clearChartButton.disabled = true;
      exportResultsButton.disabled = true;
      break;
    }
    case State.Finished:
    case State.Trapped: {
      playButton.disabled = true;
      pauseButton.disabled = true;
      stepButton.disabled = true;
      resetButton.disabled = false;
      completeButton.disabled = true;
      mInput.disabled = true;
      intervalSlider.disabled = true;
      nameInput.disabled = true;
      widthInput.disabled = true;
      heightInput.disabled = true;
      placeDoorsButton.disabled = true;
      placeObstaclesButton.disabled = true;
      placePedestriansButton.disabled = true;
      clearDoorsButton.disabled = true;
      clearObstaclesButton.disabled = true;
      clearPedestriansButton.disabled = true;
      placeRandomPedestriansButton.disabled = true;
      nInput.disabled = true;
      panicInput.disabled = true;
      exportButton.disabled = true;
      clearChartButton.disabled = true;
      exportResultsButton.disabled = true;
      break;
    }
  }
}

/**
 * Draws the main room canvas.
 */
function draw(): void {
  // Clear the canvas.
  ctx.clearRect(0, 0, snapshot.width * PPC, snapshot.height * PPC);

  // Loop through each cell.
  for (var i = 0; i < snapshot.width; i++) {
    for (var j = 0; j < snapshot.height; j++) {
      let cell = new Cell(i, j);

      // Draw cell borders.
      ctx.strokeRect(i * PPC, j * PPC, PPC, PPC);

      // If cell is pedestrian, draw pedestrian circle.
      if (_.find(snapshot.pedestrians, cell) != undefined) {
        ctx.beginPath();
        ctx.arc(i * PPC + PPC / 2, j * PPC + PPC / 2, PPC / 3, 0, 2 * Math.PI);
        ctx.stroke();
        // If cell is on perimeter and not a door or cell is obstacle, draw
        // wall.
      } else if (
        (Cell.onPerimeter(snapshot.width, snapshot.height, cell) &&
          _.find(snapshot.doors, cell) == undefined) ||
        _.find(snapshot.obstacles, cell) != undefined
      ) {
        ctx.save();
        ctx.fillStyle = "#999999";
        ctx.fillRect(i * PPC, j * PPC, PPC, PPC);
        ctx.restore();
      }
    }
  }
}

/**
 * If simulation has just finished, plot on graph.
 */
function updateGraph(): void {
  if (snapshot.state == State.Finished) {
    // Data structure that chart.js expects holding chart data.
    let data: { x: number; y: number }[] = [];

    // Data structure that chart.js expects to hold point radii.
    let pointRadius: number[] = [];

    // If only one simulation has finished, map single record to chart.js data
    // structure.
    if (snapshot.pedestrianCountsAtTime.length == 1) {
      data = snapshot.pedestrianCountsAtTime[0].map((count, i) => ({
        x: i,
        y: count,
      }));

      // Set all points to same size.
      pointRadius = Array(snapshot.pedestrianCountsAtTime[0].length).fill(3);
      // Otherwise, calculate average pedestrian count for each time period and
      // then map to chart.js structure.
    } else {
      // Get lengths of each simulation.
      let lengths = snapshot.pedestrianCountsAtTime.map(
        (pedestrianCountAtTime) => pedestrianCountAtTime.length
      );

      // Sort, longest length first.
      lengths.sort((a, b) => b - a);

      // Fill the point radii and set average time to larger point radius.
      pointRadius = Array(lengths[0]).fill(3);
      pointRadius[Math.round(snapshot.averageTime!)] = 6;

      // Loop through time steps, 0 to longest simulation length
      for (let i = 0; i < lengths[0]; ++i) {
        let currentTotal = 0;

        // Add the pedestrian count at time i from each simulation to the
        // current total. If a simulation didn't run at time i, then add 0.
        snapshot.pedestrianCountsAtTime.forEach((pedestrianCountAtTime) => {
          currentTotal +=
            pedestrianCountAtTime[i] != undefined
              ? pedestrianCountAtTime[i]
              : 0;
        });

        // Add the current time step to the chart.js data structure, calculating
        // the average.
        data.push({
          x: i,
          y: currentTotal / snapshot.pedestrianCountsAtTime.length,
        });
      }
    }

    // Add dataset to chart.
    chart.data.datasets.push({
      label: `${
        // Generate a simulation name if one not given in name input.
        nameInput.value != ""
          ? nameInput.value
          : `Simulation ${chart.data.datasets.length + 1}`
      } ${
        // Append (avg. of n) message for when multiple simulations were run.
        snapshot.pedestrianCountsAtTime.length == 1
          ? ""
          : `(avg. of ${snapshot.pedestrianCountsAtTime.length})`
      }`,
      // Generate color based on pre-defined list.
      backgroundColor: COLORS[chart.data.datasets.length % COLORS.length],
      borderColor: COLORS[chart.data.datasets.length % COLORS.length],
      data: data,
      pointRadius: pointRadius,
    });

    // Update chart to plot new datasets.
    chart.update();
  }
}

/**
 * Gets the message to be displayed in the status box depending on the current
 * state.
 * @returns Current status message.
 */
function getStatusMessage(): string {
  switch (snapshot.state) {
    case State.Doors: {
      return "Click on perimeter cells to toggle doors.";
    }
    case State.Obstacles: {
      return "Click on cells to toggle obstacles.";
    }
    case State.Pedestrians: {
      return "Click on cells to toggle pedestrians.";
    }
    case State.Running: {
      return 'Simulation running. Press "Pause" to interupt.';
    }
    case State.Paused: {
      return 'Simulation paused. Press "Step" to step through or "Play" to resume.';
    }
    case State.Completing: {
      return "Simulation completing...";
    }
    case State.Finished: {
      return 'Simulation finished. Press "Reset" to restart.';
    }
    case State.Trapped: {
      return 'There are pedestrians who do not have a route to an exit. Press "Reset" to restart.';
    }
  }
}
