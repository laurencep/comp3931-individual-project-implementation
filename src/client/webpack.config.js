const path = require("path");

module.exports = {
  entry: "./src/client/script.ts",
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  output: {
    filename: "script.js",
    path: path.resolve(__dirname, "../../dist/client"),
  },
  mode: "none",
  stats: "errors-only",
};
