import express from "express";
import _ from "lodash";
import { createServer } from "http";
import { Server, Socket } from "socket.io";
import * as path from "path";
import * as fs from "fs";
import Configuration from "../classes/Configuration";
import Client from "../classes/Client";
import multer from "multer";

// Initialise server, socket.io and file upload.
const app = express();
const port = process.env.PORT || 8000;
const server = createServer(app);
const io = new Server(server);
const upload = multer({ dest: "uploads/" });

// Holds the currently connected clients.
let clients: Client[] = [];

// Setup static file middleware.
app.use(express.static(path.join(__dirname, "../client")));

/**
 * Handles requests to the root route, sends the index HTML file.
 */
app.get("/", (req, res) => {
  res.sendFile("./index.html", {
    root: path.join(__dirname, "../client"),
  });
});

/**
 * Handles requests to the export route, sends a JSON file representing the
 * current configuration.
 */
app.get("/export/:socketID", (req, res) => {
  // Generate snapshot and write JSON to file.
  let client = clients.find(
    (client) => client.socketID == req.params.socketID
  )!;
  res.json(client.configuration.snapshot());
});

// /**
//  * Handles requests to the import route, creates a confiuration from the
//  * imported file.
//  */
app.post("/import/:socketID", upload.single("configuration"), (req, res) => {
  // Get the content of the file
  let content = fs.readFileSync(req.file!.path);

  // Try and create a configuration from the file.
  try {
    let configuration = Configuration.import(JSON.parse(content.toString()));

    // Find the client and set the configuration to be the one parsed from the
    // file.
    let client = clients.find(
      (client) => client.socketID == req.params.socketID
    )!;
    client.configuration = configuration;

    // Return success code.
    res.sendStatus(200);

    // Catch any parsing errors and return error message.
  } catch (e) {
    res.status(400);
    res.send("Invalid configuration file.");
  }

  // Delete file.
  fs.unlinkSync(req.file!.path);
});

/**
 * Handles the connection of a client.
 */
io.on("connection", (socket: Socket) => {
  // Create a new client and add it to the list.
  clients.push(new Client(socket));

  // When client disconnets, remove the client object from array of current
  // clients.
  socket.on("disconnect", () => {
    clients.filter((client) => client.socketID != socket.id);
  });
});

// Listen for HTTP requests on port.
server.listen(port, () => {
  return console.log(`server is listening on ${port}`);
});
